<?php

namespace Drupal\weeks_ago_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the weeks ago formatter.
 *
 * @FieldFormatter(
 *   id = "weeks_ago",
 *   label = @Translation("Weeks ago"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class WeeksAgoFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {

      $timestamp = $item->date->getTimestamp();

      if (time() < $timestamp) {
        $elements[$delta] = ['#markup' => $item->value . ' ' . $this->t('(this date has not come yet)')];
      }
      else {
        // 604800 - number of seconds in a week.
        $weeks_passed = floor((time() - $timestamp) / 604800);
        $elements[$delta] = ['#markup' => $weeks_passed];
      }
    }

    return $elements;
  }

}
